from django.test import TestCase
from django.urls import reverse
from .models import Product, Customer, Vendor, Category
from .views import login_user, cart 
from django.contrib.auth.models import User
from django.contrib.messages import get_messages
from django.contrib.auth import authenticate, login
from django.core.files.uploadedfile import SimpleUploadedFile




class CartTests(TestCase):
    def test_cart_unauthenticated_user(self):
        # Access the cart view without login
        response = self.client.get(reverse('cart'))

        # Check response status
        self.assertEqual(response.status_code, 200)

        # Check the context data
        self.assertEqual(len(response.context['items']), 0)
        self.assertDictEqual(response.context['order'], {'get_cart_total': 0, 'get_cart_items': 0, 'shipping': False})
        self.assertEqual(response.context['cartItems'], 0)
        
        


class LoginUserTestCase(TestCase):
    def setUp(self):
        # Set up a user for testing the login
        self.user = User.objects.create_user(username='testuser', password='testpass123')
        # Assuming you have a Customer model related to User
        self.customer = Customer.objects.create(user=self.user)  # Make sure this matches your model's requirements
        self.login_url = reverse('login')  # Make sure this is the name of your login URL

    def test_login_success(self):
        # Test a successful login
        response = self.client.post(self.login_url, {'username': 'testuser', 'password': 'testpass123'})
        # Check redirect to the home page
        self.assertRedirects(response, reverse('Home'))

        # Check login message
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(str(messages[0]), "You Have Been Logged In Successfully!!")

    def test_login_failure(self):
        # Test login with incorrect credentials
        response = self.client.post(self.login_url, {'username': 'testuser', 'password': 'wrongpassword'})
        # Should redirect back to login
        self.assertRedirects(response, reverse('login'))

        # Check failure message
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(str(messages[0]), "Incorrect password or username, please try again")




class CustomerModelTest(TestCase):
    def setUp(self):
        # Create a user for linking to the Customer
        self.user = User.objects.create_user(username='testuser', password='testpassword123')
    
    def test_create_customer(self):
        # Create a Customer instance and verify its fields
        customer = Customer.objects.create(
            user=self.user,
            first_name="John",
            last_name="Doe",
            phone="1234567890",
            email="john.doe@example.com",
            password="securepassword123"  # This is not typically recommended, better to use User model for password management
        )

        self.assertEqual(customer.user.username, 'testuser')
        self.assertEqual(customer.first_name, 'John')
        self.assertEqual(customer.last_name, 'Doe')
        self.assertEqual(customer.phone, '1234567890')
        self.assertEqual(customer.email, 'john.doe@example.com')
        self.assertEqual(customer.password, 'securepassword123')

    def test_customer_string_representation(self):
        # Test the string representation of the Customer model
        customer = Customer(
            first_name="Jane",
            last_name="Smith"
        )
        self.assertEqual(str(customer), 'Jane Smith')

    def test_delete_user_cascade_to_customer(self):
        # Test that deleting the User also deletes the associated Customer
        customer = Customer.objects.create(
            user=self.user,
            first_name="John",
            last_name="Doe"
        )
        # Delete the user
        self.user.delete()
        # Check that the customer is also deleted
        self.assertFalse(Customer.objects.filter(first_name="John").exists())


